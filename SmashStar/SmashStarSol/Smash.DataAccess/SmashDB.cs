﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smash.DataAccess
{
    public class SmashDB
    {
        SmashDBEntities sdb = new SmashDBEntities();
        public int SubmitVideo(SmashData data)
        {

            //-2 - url already added
            //-3 - title 
            //-1 failed
            if (sdb.Videos.FirstOrDefault(v => v.URL.Equals(data.video.URL)) != null)
            {
                return -2;
            }
            if (sdb.Videos.FirstOrDefault(v => v.Title.Equals(data.video.Title)) != null)
            {
                return -3;
            }
            //try
            //{
            sdb.Videos.Add(data.video);
            sdb.SaveChanges();
            return 1;
            //}
            //catch (Exception)
            //{
            //    return -1;
            //}
        }

        public Player GetPlayer(SmashData data)
        {
            Player player = sdb.Players.FirstOrDefault(p => p.Name.Equals(data.player1.Name));
            return player;
        }

        public Dictionary<string, int> GetAllPlayers()
        {
            Dictionary<string, int> ret = new Dictionary<string, int>();
            foreach (var item in sdb.Players)
            {
                //
                ret.Add(item.Name, item.PlayerId);
            }
            return ret;
        }

        public IEnumerable<ValidVideo> GetValidVideos()
        {
            return sdb.ValidVideos.ToList();
        }
    }
}

