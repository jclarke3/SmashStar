﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smash.DataAccess
{
    public class SmashData
    {
        public Video video { get; set; }
        public Player player1 { get; set; }
        public Player player2 { get; set; }
        public Tournament tournament { get; set; }

    }
}
