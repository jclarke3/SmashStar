﻿using Smash.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Smash.Domain
{
    public class DomainHelper
    {
        SmashDB sdb = new SmashDB();
        public int Submit(SubmitDAO submission)
        {
            SmashData data = new SmashData();
            data.video.Active = false;
            data.video.Category = submission.Category;
            data.video.Char1 = submission.Char1;
            data.video.Char2 = submission.Char2;
            data.video.DateAdded = DateTime.Today;

            data.player1.Name = submission.Player1;
            data.player2.Name = submission.Player2;
            return sdb.SubmitVideo(data);
        }

        public Dictionary<string, int> GetAllPlayers()
        {
            return sdb.GetAllPlayers();
        }

        public IEnumerable<VideoDAO> GetAllVideos()
        {
            var vids = sdb.GetValidVideos();
            List<VideoDAO> ret = new List<VideoDAO>();
            foreach (var item in vids)
            {
                ret.Add(new VideoDAO()
                {
                    Category = item.Category,
                    Char1 = item.Char1,
                    Char2 = item.Char2,
                    DateAdded = item.DateAdded,
                    Description = item.Description,
                    Active = true,
                    Player1Id = item.Player1Id,
                    Player2Id = item.Player2Id,
                    Source = item.Source,
                    SubmitContact = item.SubmitContact,
                    SubmitName = item.SubmitName,
                    Title = item.Title,
                    TournamentId = item.TournamentId,
                    URL = item.URL,
                    VideoId = item.VideoId
                });
            }
            return ret;
        }
    }
}

