create schema Smash;
go


create table Smash.Video(
  VideoId int not null identity(1,1),
  Title nvarchar(50) not null,
  [Description] nvarchar(200) not null,
  URL nvarchar(100) not null,
  Player1Id int not null,
  Player2Id int null,
  Char1 nvarchar(30) not null,
  Char2 nvarchar(30) not null,
  [Source] nvarchar(50) not null,
  Category nvarchar(50) not null,
  TournamentId int not null,
  SubmitName nvarchar(50) not null,
  SubmitContact nvarchar(50) not null,
  Active bit not null,
  DateAdded date not null
)

drop table Smash.Player;
create table Smash.Player(
  PlayerId int not null identity(1,1),
  Name nvarchar(50) not null,
  FirstName nvarchar(50) not null,
  LastName nvarchar(50) not null,
  Sponsor nvarchar(50) null,
  SponsorShort nvarchar(10) null,
  Location nvarchar(50) not null,
  MainChar nvarchar(30) not null,
  DualChar nvarchar(30) null,
  PGRank int null,
  PowerRank int null,
  Twitter nvarchar(50) null,
  Twitch nvarchar(50) null
)

create table Smash.Tournament(
  TournamentId int not null identity(1,1),
  Name nvarchar(50) not null,
  [Date] date not null,
  NumEntrants int null,
  SeriesId int not null,
  P1 int null,
  P2 int null,
  P3 int null,
  P4 int null,
  P5 int null,
  P6 int null,
  P7 int null,
  P8 int null,
  P9 int null,
  P10 int null,
  P11 int null,
  P12 int null,
  P13 int null,
  P14 int null,
  P15 int null,
  P16 int null
)

create table Smash.Series(
  SeriesId int not null,
  Name nvarchar(50)
)

--set the primary keys
alter table Smash.Video
add constraint pk_video_videoid primary key clustered (VideoId);

alter table Smash.Player
add constraint pk_player_playerid primary key clustered (PlayerId);

alter table Smash.Tournament
add constraint pk_tournament_tournamentid primary key clustered (TournamentId);

alter table Smash.Series
add constraint pk_series_seriesid primary key clustered (SeriesId);


create View Smash.ValidVideo as 
select VideoId, Title, [Description], URL, Player1Id, Player2Id, Char1, Char2, [Source], Category, TournamentId, SubmitName,
 SubmitContact,DateAdded
  from Smash.Video where Active = 1;
go

select * from Smash.Video;
select * from Smash.Player;
select * from Smash.Tournament;
insert into Smash.Player(Name, FirstName, LastName, Sponsor, SponsorShort, Location, MainChar, DualChar, PGRank, PowerRank, Twitter, Twitch)
 values ('ZeRo', 'Gonzalo', 'Barrio', 'Team SoloMid', 'TSM', 'SoCal', 'Diddy Kong', 'Sheik', 1,1,'@zero', 'zero');
insert into Smash.Player(Name, FirstName, LastName, Sponsor, SponsorShort, Location, MainChar, PGRank, PowerRank, Twitter, Twitch)
 values ('Nairo', 'Nairoby', 'Quezada', 'Team Liquid', 'Liquid', 'Tri-State', 'Zero Suit Samus',  2,1,'@nairo', 'nairo');
insert into Smash.Player(Name, FirstName, LastName, Sponsor, SponsorShort, Location, MainChar,DualChar, PGRank, PowerRank, Twitter, Twitch)
 values ('False', 'Corey', 'Shin', 'Leap of Faith', 'LoF', 'Tri-State', 'Sheik','Marth',  35,4,'@falsify', 'http://www.twitch.tv/rushhoursmash');
insert into Smash.Player(Name, FirstName, LastName, Sponsor, SponsorShort, Location, MainChar,DualChar, PGRank, PowerRank, Twitter, Twitch)
 values ('ESAM', 'Eric', 'Lew', 'Panda Global', 'PG', 'Florida', 'Pikachu','Corrin',  14,1,'@pg_esam', 'http://www.twitch.tv/trixiesam');
insert into Smash.Player(Name, FirstName, LastName, Sponsor, SponsorShort, Location, MainChar, PGRank, PowerRank, Twitter)
 values ('Void', 'James', 'Makekau-Tyson', 'CounterLogic Gaming', 'CLG', 'Socal', 'Sheik', 8,2,'@gsmVoiD');

 insert into Smash.Video(Title, [Description], URL, Player1Id, Char1,Char2, TournamentId, [Source], Category, SubmitName, SubmitContact, Active, DateAdded)
  values('Press Z to Win', 'Falcon gets bodied and flushed', 'https://twitter.com/fals1fy/status/750405290349174784', 3,'Luigi',
    'Captain Falcon', 1, 'twitter','combo', 'False', '@falsify', 1, GETDATE())

 insert into Smash.Video(Title, [Description], URL, Player1Id, Char1, Char2, TournamentId, [Source], Category, SubmitName, SubmitContact, Active, DateAdded)
  values('Smash 4 Metagame (EVO IS GONNA BE CRAZY!)', 'ESAM explains meta', 'https://www.youtube.com/watch?v=ezrj9RUUCCw', 4,'core',
    'none', 1, 'youtube','other', 'ESAM', '@pg_esam', 1, GETDATE())

 insert into Smash.Video(Title, [Description], URL, Player1Id, Char1, Char2, TournamentId, [Source], Category, SubmitName, SubmitContact, Active, DateAdded)
  values('Voids Textbook Edgeguard against Fox', 'Disgusting', 'https://oddshot.tv/shot/2GGaming/UzqD8G9eD8FIaY1pd93xFaNf', 5,'sheik',
    'fox', 1, 'oddshot','combo', 'Void', '@gsmvoid', 1, GETDATE())

insert into Smash.Tournament(Name, [Date], NumEntrants, SeriesId)
  values ('NA', GETDATE(), 0, 0);
 update Smash.Player set Location = 'Tri-State' where PlayerId = 2;
 update Smash.Video set Char1 = 'other' where VideoId = 4;